import React from 'react';
import Nav from './Nav';
import NoteList from './NoteList';
import NoteForm from './NoteForm';
import NavURL from './NavURL';
import { connect } from 'react-redux';
class Home extends React.Component {

    showForm = () => {
        if (this.props.isEdit) {
            return <NoteForm />
        }
    }

    render() {
        return (
            <div>
                <Nav/>               
                <div className="container">    
                    <div className="row"> 
                     <NoteList/>                      
                        {
                            this.showForm()
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
      isEdit: state.isEdit
    }
  }
  export default connect(mapStateToProps)(Home)