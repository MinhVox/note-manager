import React from 'react';
import './App.css';
import { Helmet } from "react-helmet";
import AlertInfo from './AlertInfo';
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import NavURL from './NavURL';
import Login from './Login';
import Home from './Home';
import {connect} from 'react-redux';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  Login = () => {
    if (this.props.login) {
        return <Home />
    }else{
      return <Login/>
    }
}

  render() {
    return (
      <Router>
        <div>
          <NavURL/>
          <AlertInfo />          
          {
            this.Login()  
          }
          <Helmet>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
          </Helmet>
        </div>
      </Router >
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    login: state.login
  }
}
export default connect(mapStateToProps)(App)
