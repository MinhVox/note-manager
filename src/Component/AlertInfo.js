import React from 'react';
import { AlertContainer, Alert } from 'react-bs-notifier'
import { connect } from 'react-redux'
class AlertInfo extends React.Component {
    handleDismiss = () => {
        this.props.alertOff();
    }
    render() {
        if (this.props.AlertShow === false) return null;
        return (
            <AlertContainer>
                <Alert type={this.props.AlertType} onDismiss={() => this.handleDismiss()} timeout={2000}>{this.props.AlertContent}</Alert>
            </AlertContainer>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        AlertShow: state.alertShow,
        AlertContent: state.alertContent,
        AlertType: state.alertType,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        alertOff: () => {
            dispatch({
                type: "ALERT_OFF"
            })
        },
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AlertInfo)