import React from 'react';
import {connect} from 'react-redux'

class NoteItem extends React.Component {

    editButtonClick = () => {       
        this.props.changeEditStatus();
        this.props.getEditData(this.props.note);
    }

    removeButtonClick = () =>{
        this.props.removeData(this.props.note);
        this.props.alertOn("Xoá thành công","danger");
    }

    render() {
        return (
            <div>
                 <div className="card">
                        <div className="card-header" role="tab" id="note1">
                            <h5 className="mb-0">
                                <a data-toggle="collapse" data-parent="#noteList" href={"#N"+ this.props.i} aria-expanded="true" aria-controls="section1ContentId">
                                    {this.props.noteTitle}
          </a>
          <div class="btn-group float-right">
              <button className="btn btn-outline-info" onClick={() => this.editButtonClick()}>Sửa</button>
              <button className="btn btn-outline-secondary" onClick={() => this.removeButtonClick()}>Xoá</button>
          </div>
                            </h5>
                        </div>
                        <div id={"N" + this.props.i} className="collapse in" role="tabpanel" aria-labelledby="note1">
                            <div className="card-body">
                                {this.props.noteContent}
        </div>
                        </div>
                    </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        editStatus: state.isEdit
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changeEditStatus: () => {
            dispatch({
                type:"CHANGE_EDIT_STATUS"
            })
        },
        getEditData: (editObject) => {
            dispatch({
                type:"GET_EDIT_DATA",editObject
            })
        },
        removeData: (editObject) => {
            dispatch({
                type:"REMOVE",editObject
            })
        },
        alertOn: (AlertContent,AlertType) => {
            dispatch({
                type: "ALERT_ON",AlertContent,AlertType
            })
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NoteItem)