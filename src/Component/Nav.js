import React from 'react';
import { connect } from 'react-redux'
import {  NavLink } from "react-router-dom";
class Nav extends React.Component {
    handleAdd = (event) => {

        event.preventDefault();
        this.props.changeEditStatus();
        this.props.changeAddStatus();
        
    }

    handleAdd = (event) => {

        event.preventDefault();
        this.props.logout();  
        this.props.alertOn("Thoát thành công", "success");     
    }

    render() {
        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-primary mb-4">
                <a className="navbar-brand" >My Note</a>
                <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation" />
                <div className="collapse navbar-collapse justify-content-end" id="collapsibleNavId">
                    <ul className="navbar-nav  mt-2 mt-lg-0 ">
                        <li className="nav-item active">
                            <a className="nav-link" href="www.google.com" onClick={(event) => this.handleAdd(event)}>Logout</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="www.google.com" onClick={(event) => this.handleAdd(event)}>Add NOTE</a>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        prop: state.prop,
        editStatus: state.isEdit,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changeEditStatus: () => {
            dispatch({
                type: "CHANGE_EDIT_STATUS"
            })
        },
        changeAddStatus: () => {
            dispatch({
                type: "CHANGE_ADD_STATUS"
            })
        },
        logout: () => {
            dispatch({
                type: "LOGOUT"
            })
        },
        alertOn: (AlertContent, AlertType) => {
            dispatch({
                type: "ALERT_ON", AlertContent, AlertType
            })
        },

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav)