import { noteData, account } from "./firebaseConnect"

var redux = require("redux");

const noteInitialState = {
    isEdit: false,
    editItem: {},
    isAdd: false,
    alertShow: false,
    alertContent: '',
    alertType: '',
    login: false,
};
const allReducer = (state = noteInitialState, action) => {
    switch (action.type) {
        case "ADD_DATA":
            noteData.push(action.getItem)
            console.log("Thêm dự liệu" + JSON.stringify(action.getItem) + " thành công");
            return state;
        case "CHANGE_EDIT_STATUS":
            if (state.isAdd === true) {
                return { ...state, isEdit: !state.isEdit, isAdd: !state.isAdd }
            } else {
                return { ...state, isEdit: !state.isEdit }
            }
        case "CHANGE_ADD_STATUS":
            return { ...state, isAdd: !state.isAdd, editItem: {} }
        case "GET_EDIT_DATA":
            return { ...state, editItem: action.editObject }
        case "EDIT":
            noteData.child(action.getItem.id).update({
                noteTitle: action.getItem.noteTitle,
                noteContent: action.getItem.noteContent
            })
            return { ...state, editItem: {} }
        case "REMOVE":
            noteData.child(action.editObject.id).remove();
            return state;
        case "ALERT_ON":
            return { ...state, alertShow: true, alertContent: action.AlertContent, alertType: action.AlertType }
        case "ALERT_OFF":
            return { ...state, alertShow: false }
        case "LOGIN":
            return { ...state, login: true }
        case "LOGOUT":
            return { ...state, login: false }
        default:
            return state;
    }
}
var store = redux.createStore(allReducer);
store.subscribe(function () {
    console.log(JSON.stringify(store.getState()))
})
export default store;