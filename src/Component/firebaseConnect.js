import * as firebase from 'firebase';
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB2PCazRiDRlMMje3WIJReuNxGyXmTvvVY",
    authDomain: "notereact-e808e.firebaseapp.com",
    databaseURL: "https://notereact-e808e.firebaseio.com",
    projectId: "notereact-e808e",
    storageBucket: "",
    messagingSenderId: "314119087470",
    appId: "1:314119087470:web:1e0f57d5312f35c82a4d7f"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig)
export const noteData = firebase.database().ref("dataForNote");
export const account = firebase.database().ref("Account");