import React from 'react';
import { connect } from 'react-redux'
// import { noteData } from './firebaseConnect';

class NoteForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            noteTitle: '',
            noteContent: '',
            id: ''
        }
    }


    componentWillMount() {
        if (this.props.editItem) {
            this.setState({
                noteTitle: this.props.editItem.noteTitle,
                noteContent: this.props.editItem.noteContent,
                id: this.props.editItem.id
            })
        }
    }


    isChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        })
    }

    saveData = (title, content) => {
        if (this.state.id) {
            var editObject = {};
            editObject.id = this.state.id;
            editObject.noteContent = this.state.noteContent;
            editObject.noteTitle = this.state.noteTitle;

            this.props.editDataStore(editObject);
            this.props.changeEditStatus();
            this.props.alertOn("Sửa thành công", "info");
        }
        else {
            var a = title || '';
            var b = content || '';
            if (a.length === 0 || b.length === 0) {
                this.props.alertOn("Không được để trống", "warning");
            } else {
                var item = {};
                item.noteTitle = title;
                item.noteContent = content;
                // //send item
                this.props.addDataStore(item); 
                this.props.alertOn("Thêm mới thành công", "success");
            }
        }
    }

    printTitle = () => {
        if (this.props.addStatus) {
            return <h3>Thêm mới Note</h3>
        } else {
            return <h3>Sửa Note</h3>
        }

    }
    render() {
        //check có connect đc ko ?
        // noteData.once("value").then(function(snapshot){
        //     console.log(snapshot.val());
        // });


        return (
            <div className="col-4">
                {this.printTitle()}
                <form>
                    <div className="form-group">
                        <label htmlFor="noteTitle">Tiêu đề Note</label>
                        <input defaultValue={this.props.editItem.noteTitle} onChange={(event) => this.isChange(event)} type="text" className="form-control" name="noteTitle" id="noteTitle" aria-describedby="helpIdNoteTitle" placeholder="Tiêu đề Note" required />
                        <small id="helpIdNoteTitle" className="form-text text-muted">Điền tiêu đề vào đây</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="noteContent">Nội dung Note</label>
                        <textarea defaultValue={this.props.editItem.noteContent} onChange={(event) => this.isChange(event)} type="text" className="form-control" name="noteContent" id="noteContent" aria-describedby="helpIdNoteContent" placeholder="Nội dung Note" required />
                        <small id="helpIdNoteContent" className="form-text text-muted">Điền tiêu đề vào đây</small>
                    </div>
                    <button type="reset" className="btn btn-primary" onClick={() => this.saveData(this.state.noteTitle, this.state.noteContent)}>Lưu</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        editItem: state.editItem,
        addStatus: state.isAdd,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        addDataStore: (getItem) => {
            dispatch({ type: "ADD_DATA", getItem })
        },
        editDataStore: (getItem) => {
            dispatch({ type: "EDIT", getItem })
        },
        changeEditStatus: () => {
            dispatch({
                type: "CHANGE_EDIT_STATUS"
            })
        },
        alertOn: (AlertContent, AlertType) => {
            dispatch({
                type: "ALERT_ON", AlertContent, AlertType
            })
        },
        alertOff: () => {
            dispatch({
                type: "ALERT_OFF"
            })
        },
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(NoteForm)