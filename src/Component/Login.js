import React from 'react';
import { account } from './firebaseConnect';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
        this.CheckLogin = this.CheckLogin.bind(this);
    }

    isChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value,
            redirect: false
        })
    }

    CheckLogin = async (username,password) => {
        let isSuccess = await account.child(username).once("value").then((data) => {
            if(data.val() != null){
                if(data.child('Password').val() == password){
                    return true;
                }
                return false;
            }
        })
        if (isSuccess) {
            this.props.alertOn("Đăng nhập thành công", "success");
            this.props.login();
        }else{
            this.props.alertOn("Sai tài khoản hoặc mật khẩu", "warning");
        }
    }
    render() {
        // if (this.state.redirect) {
        //     return <Redirect to="/Home"/>
        // }
        return (
            <div className="col-md-5 mx-auto">
                <div id="first">
                    <div className="myform form ">
                        <div className="logo mb-3">
                            <div className="col-md-12 text-center">
                                <h1>Login</h1>
                            </div>
                        </div>
                        <form>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Username</label>
                                <input type="text" onChange={(event)=>this.isChange(event)} name="username" className="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter Username" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Password</label>
                                <input type="password" onChange={(event)=>this.isChange(event)} name="password" id="password" className="form-control" aria-describedby="emailHelp" placeholder="Enter Password" />
                            </div>
                            <div className="col-md-12 text-center ">
                                <button type="reset" onClick={()=>this.CheckLogin(this.state.username,this.state.password)} className=" btn btn-block mybtn btn-primary tx-tfm">Login</button>
                            </div>
                            {/* <div className="form-group">
                                <p className="text-center">Don't have account? <a href="#" id="signup">Sign up here</a></p>
                            </div> */}
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        alertOn: (AlertContent, AlertType) => {
            dispatch({
                type: "ALERT_ON", AlertContent, AlertType
            })
        },
        login :() =>{
            dispatch({
                type: "LOGIN"
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)