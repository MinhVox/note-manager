import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from './Login';
import Home from './Home';

class NavURL extends React.Component {
    render() {
        return (
            <div className="col">
                <Route exact path="/lg"  component={Login}/>
            </div>
        );
    }
}

export default NavURL;